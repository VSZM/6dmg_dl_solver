#!/usr/bin/python
# encoding: utf-8
#Author: Szegedi Gábor


import re	
import scipy.io
import numpy
import h5py
import argparse
import os
import sys
import random as rnd

from pylab import *
from numpy import *
from mpl_toolkits.mplot3d import axes3d


"""
1. Each .mat file is named as g[XX]_[YY]_t[ZZ].mat:
[XX]: gesture index
[YY]: tester ID
[ZZ]: trial index
"""
CHAR_MAT_PATTERN = "[ -~]*(?P<gesture_idx>lower_a|lower_b|lower_c|lower_d|lower_e|lower_f|lower_g|lower_h|lower_i|lower_j|lower_k|lower_l|lower_m|lower_n|lower_o|lower_p|lower_q|lower_r|lower_s|lower_t|lower_u|lower_v|lower_w|lower_x|lower_y|lower_z|num_0|num_1|num_2|num_3|num_4|num_5|num_6|num_7|num_8|num_9|upper_A|upper_B|upper_C|upper_D|upper_E|upper_F|upper_G|upper_H|upper_I|upper_J|upper_K|upper_L|upper_M|upper_N|upper_O|upper_P|upper_Q|upper_R|upper_S|upper_T|upper_U|upper_V|upper_W|upper_X|upper_Y|upper_Z)_(?P<tester_id>[a-zA-Z0-9]+)_t(?P<trial_idx>[0-9]+).mat"
GEST_MAT_PATTERN = "[ -~]*g(?P<gesture_idx>[0-9]+)_(?P<tester_id>[a-zA-Z0-9]+)_t(?P<trial_idx>[0-9]+).mat"
motion_mat_matcher = re.compile(GEST_MAT_PATTERN)
char_mat_matcher = re.compile(CHAR_MAT_PATTERN)

"""a = numpy.array([[1,2][3,4]])
a = numpy.array([])
a
a = numpy.array([1,2,3])
b = numpy.array([0,2,1])
c = numpy.array([a,b])
c
d = numpy.array([c, c])
d
d.shape
c = numpy.array([[a,b]])
d= numpy.array([c,c,c,c,c])
d.shape
"""

MOTION_INDEX_TO_NAME = [
"SWIPE_RIGHT",
"SWIPE_LEFT",
"SWIPE_UP",
"SWIPE_DOWN",
"SWIPE_UPRIGHT",
"SWIPE_UPLEFT",
"SWIPE_DOWNRIGHT",
"SWIPE_DOWNLEFT",
"POKE_RIGHT",
"POKE_LEFT",
"POKE_UP",
"POKE_DOWN",
"V_SHAPE",
"X_SHAPE",
"CIRCLE_HORIZONTAL_CLOCKWISE",
"CIR_HORIZONTAL_COUNTERCLOCKWISE",
"CIR_VERTICAL_CLOCKWISE",
"CIR_VERTICAL_COUNTERCLOCKWISE",
"TWIST_CLOCKWISE",
"TWIST_COUNTERCLOCKWISE"
]

CHAR_INDEX_TO_NAME = ["lower_a",
"lower_b",
"lower_c",
"lower_d",
"lower_e",
"lower_f",
"lower_g",
"lower_h",
"lower_i",
"lower_j",
"lower_k",
"lower_l",
"lower_m",
"lower_n",
"lower_o",
"lower_p",
"lower_q",
"lower_r",
"lower_s",
"lower_t",
"lower_u",
"lower_v",
"lower_w",
"lower_x",
"lower_y",
"lower_z",
"num_0",
"num_1",
"num_2",
"num_3",
"num_4",
"num_5",
"num_6",
"num_7",
"num_8",
"num_9",
"upper_A",
"upper_B",
"upper_C",
"upper_D",
"upper_E",
"upper_F",
"upper_G",
"upper_H",
"upper_I",
"upper_J",
"upper_K",
"upper_L",
"upper_M",
"upper_N",
"upper_O",
"upper_P",
"upper_Q",
"upper_R",
"upper_S",
"upper_T",
"upper_U",
"upper_V",
"upper_W",
"upper_X",
"upper_Y",
"upper_Z"]




#CONFIG
POS_VECTOR_LEN = 70 # Average of input length at GestureTable
TEST_DATA_PERCENTAGE = 0.3

debug = False
def normalizeVect(vec):
	posvec = [x - min(vec) for x in vec] # moving values to the [0..] range 
	
	if debug:
		print posvec
	
	indices = [i * (float(POS_VECTOR_LEN - 1) / (len(posvec) - 1)) for i in range(0,len(posvec))] # scaling the vector to POS_VECTOR_LEN
	
	if debug:
		print indices
	posvec_scaled = numpy.interp(range(0, POS_VECTOR_LEN) ,indices ,posvec)

	#return posvec_scaled
	#Scaling single values to the [0-1] interval
	return [x / max(posvec_scaled) for x in posvec_scaled]


	
		
class GestureBase:
	str_rep = None	

	def visualize(self,rootFolder):
		fig = figure()
		ax = fig.gca(projection='3d')
		ax.plot(self.x_vec,self.y_vec,self.z_vec,'-')                                             
		ax.plot(self.x_vec,self.y_vec,'-')                                                         
		ax.figure.canvas.draw()
		savefig(rootFolder + str(self) + '.png')
		matplotlib.pyplot.close(fig)
		
	def dataLabel(self):
		pos_matrix = numpy.array([self.x_vec, self.y_vec, self.z_vec])
				
		return pos_matrix.flatten('F'), float(hash(self))
	
	def __hash__(self):
		return hash(str(self))

	def __eq__(self, other):
		return str(self) == str(other)

	def __cmp__(self, other):
		if str(self) == str(other):
			return 0
		if hash(self) < hash(other):
			return -1

		return 1


class CharGesture(GestureBase):
	
	def __init__(self, mat_file_name):
		matches = char_mat_matcher.match(mat_file_name)
		self.gestureName = matches.group('gesture_idx')
		self.testerId = matches.group('tester_id')
		self.trialIndex = matches.group('trial_idx')

		mat = scipy.io.loadmat(mat_file_name)
		self.x_vec = normalizeVect(mat['gest'][1])
		self.y_vec = normalizeVect(mat['gest'][2])
		self.z_vec = normalizeVect(mat['gest'][3])
		
		self.str_rep = str(self)

	def __hash__(self):
		return CHAR_INDEX_TO_NAME.index(self.gestureName)

	def __str__(self):
		if self.str_rep is not None:
			return self.str_rep		

		return self.testerId + "__" + self.gestureName + "__" + self.trialIndex

class MotionGesture(GestureBase):
	
	def __init__(self, mat_file_name, hand):
		matches = motion_mat_matcher.match(mat_file_name)
		self.gestureName = MOTION_INDEX_TO_NAME[int(matches.group('gesture_idx'))]
		self.testerId = matches.group('tester_id')
		self.trialIndex = matches.group('trial_idx')
		self.hand = hand

		mat = scipy.io.loadmat(mat_file_name)
		self.time = normalizeVect(mat['gest'][0])
		self.x_vec = normalizeVect(mat['gest'][1])
		self.y_vec = normalizeVect(mat['gest'][2])
		self.z_vec = normalizeVect(mat['gest'][3])
		
		self.str_rep = str(self)

	
	def __hash__(self):
		return MOTION_INDEX_TO_NAME.index(self.gestureName)
		

	def __str__(self):
		if self.str_rep is not None:
			return self.str_rep		

		return self.testerId + self.hand + "__" + str(self.gestureName) + "__" + self.trialIndex



def create_hdf5(rootFolder, dataType, gestures):
	data  = []
	label = []
	for g in gestures:
		d, l = g.dataLabel()
		data.append(d)
		label.append(l)
	
	with h5py.File(os.path.join(rootFolder, dataType + ".hdf5"), 'w') as f:
		f['data']=numpy.array(data)
		f['label']=numpy.array(label)
	with open(os.path.join(rootFolder, dataType + '.txt'), 'w') as f:
		f.write(dataType + ".hdf5" + '\n')
    

def processMotionGestures(rootFolder, gestures):

	percent = 0
	for (path, dirs, files) in os.walk(rootFolder + '/matL'):
		print "Processing {} left hand samples...".format(len(files))
		i = 1
		for f in files:
			gestures.append(MotionGesture(path + "/" + f,'LH'))
			perc = int(100 * float(i) / len(files))
			i += 1
			if percent < perc:
				percent = perc
				sys.stdout.write("\r%d%%" % percent)
    			sys.stdout.flush()

	print "\nLeft hand samples processed!"
	print
			
	
	percent = 0	
	for (path, dirs, files) in os.walk(rootFolder + '/matR'):
		print "Processing {} right hand samples...".format(len(files))
		i = 1
		for f in files:
			gestures.append(MotionGesture(path + "/" + f,'RH'))
			perc = int(100 * float(i) / len(files))
			i += 1
			if percent < perc:
				percent = perc
				sys.stdout.write("\r%d%%" % percent)
    			sys.stdout.flush()
	

	print "\nRight hand samples processed!"

def processCharGestures(rootFolder, gestures):
	percent = 0
	for (path, dirs, files) in os.walk(rootFolder + '/matR_char'):
		print "Processing {} char gestures...".format(len(files))
		i = 1
		for f in files:
			gestures.append(CharGesture(path + "/" + f))
			perc = int(100 * float(i) / len(files))
			i += 1
			if percent < perc:
				percent = perc
				sys.stdout.write("\r%d%%" % percent)
    			sys.stdout.flush()

	print "\nChar gestures processed!"
	print

	
def preprocessGestureFiles(rootFolder, outputRoot, visualize, db):
	gestures = []
	
	if db == "MOTION":
		processMotionGestures(rootFolder, gestures)
	elif db == "CHAR":
		processCharGestures(rootFolder, gestures)
	else:
		raise ValueError("db parameter must be 'MOTION' or 'CHAR'")

	gestures.sort()		

	print "Creating output directory structure..."
	
	if not os.path.exists(outputRoot):
		os.makedirs(outputRoot)

	if db == "MOTION":
		visOutputFolder = outputRoot + 'visualization/motions/'		
	elif db == "CHAR":
		visOutputFolder = outputRoot + 'visualization/chars/'
	else:
		raise ValueError("db parameter must be 'MOTION' or 'CHAR'")		

	if visualize and not os.path.exists(visOutputFolder):
		os.makedirs(visOutputFolder)
		
	print "Directory structure created!"
	print
	print "Creating test and train dataset..."			
	
	test = rnd.sample(gestures, int(len(gestures) * TEST_DATA_PERCENTAGE))

	train = [g for g in gestures if g not in test]

	if db == "MOTION":
		create_hdf5(outputRoot, 'train_mg', train)
		create_hdf5(outputRoot, 'test_mg', test)
	elif db == "CHAR":
		create_hdf5(outputRoot, 'train_ch', train)
		create_hdf5(outputRoot, 'test_ch', test)
	else:
		raise ValueError("db parameter must be 'MOTION' or 'CHAR'")
		

	print "Datasets created successfully!"
	print


	if visualize:
		if db == "MOTION":
			visOutputFolder = outputRoot + 'visualization/chars/'
		elif db == "CHAR":
			visOutputFolder = outputRoot + 'visualization/motions/'		
		else:
			raise ValueError("db parameter must be 'MOTION' or 'CHAR'")

		print "Visualizing gestures to pngs..."
		percent = 0	
		i = 1
		for g in gestures:
			g.visualize(visOutputFolder)
			i += 1
			perc = int(100 * float(i) / len(gestures))
			if percent < perc:
				percent = perc
				sys.stdout.write("\r%d%%" % percent)
				sys.stdout.flush()

	print "\nPREPROCESS FINISHED SUCCESSFULLY!" 
	print		
	

if __name__=="__main__":
	parser = argparse.ArgumentParser(description='This script converts the 6DMG database to something that is more simple and usable to a caffe neural network. Author is VSZM.')
	parser.add_argument('--sourceRoot', dest='root', default='./matlab_orig', nargs='?',
					   help='The folder in which the 6DMG matlab files can be found. This is the folder which contains 2 subfolders: matL, matR')
	parser.add_argument('--outputRoot', dest='output', default='./', nargs='?',
					   help='The folder in which the newly created gestures will reside. Default is ./output')

	parser.add_argument('--db', dest='db', default='MOTION', nargs='?', choices=['MOTION','CHAR'],
				   help='Which 6dmg dataset to process into HDF5. Default is MOTION')

	parser.add_argument('--visualize', dest='visualize', default=False, nargs='?', choices=['True','False'],
				   help='Which 6dmg dataset to process into HDF5. Default is MOTION')
	
	args = parser.parse_args()
	
	preprocessGestureFiles(args.root, args.output, bool(args.visualize), args.db)
